package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

func questionHandler(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(r.PathValue("userid"))
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	if _, ok := Users[userID]; !ok {
		http.Error(w, "User not found", http.StatusBadRequest)
		return
	}

	questionID, err := strconv.Atoi(r.PathValue("questionid"))
	if err != nil {
		http.Error(w, "Invalid Question ID", http.StatusBadRequest)
		return
	}

	if questionID < 1 || questionID > 20 {
		http.NotFound(w, r)
		return
	}

	data := struct {
		UserID       int
		QuestionID   int
		QtextKorean  string
		QtextEnglish string
		Atype        string
		Answers      []string
		PrevQID      int
		NextQID      int
	}{
		UserID:       userID,
		QuestionID:   questionID,
		QtextKorean:  Questions[questionID].QtextKorean,
		QtextEnglish: Questions[questionID].QtextEnglish,
		Atype:        Questions[questionID].Atype,
		Answers:      Users[userID].Answers[questionID],
		PrevQID:      max(1, questionID-1),
		NextQID:      min(MAX_QNUM, questionID+1),
	}
	tmpl, err := template.ParseFiles("html/question.html")
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	tmpl.Execute(w, data)
}

func submitAnswerHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Only POST method is allowed", http.StatusMethodNotAllowed)
		return
	}

	if err := r.ParseForm(); err != nil {
		http.Error(w, "Error parsing form", http.StatusBadRequest)
		return
	}

	userIDStr := r.Form.Get("userID")
	questionIDStr := r.Form.Get("questionID")

	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	if _, ok := Users[userID]; !ok {
		http.Error(w, "UserID not found", http.StatusInternalServerError)
		return
	}

	questionID, err := strconv.Atoi(questionIDStr)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	var answers []string
	for i := 0; i < Questions[questionID].Anum; i++ {
		answer := r.Form.Get(fmt.Sprintf("answer%d", i))
		answers = append(answers, answer)
	}

	newAnswer := Users[userID].Answers
	newAnswer[questionID] = answers
	Users[userID] = User{
		UserID:   Users[userID].UserID,
		RealName: Users[userID].RealName,
		NickName: Users[userID].NickName,
		Answers:  newAnswer,
		Guesses:  Users[userID].Guesses,
	}

	var redirectAddr string
	if r.FormValue("action") == "previous" {
		redirectAddr = fmt.Sprintf("/user/%d/question/%d", userID, max(1, questionID-1))
	} else if r.FormValue("action") == "next" {
		redirectAddr = fmt.Sprintf("/user/%d/question/%d", userID, min(MAX_QNUM, questionID+1))
	} else if r.FormValue("action") == "submit" {
		redirectAddr = fmt.Sprintf("/thank-you/%d", userID)
	}
	http.Redirect(w, r, redirectAddr, http.StatusSeeOther)
}
