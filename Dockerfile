FROM golang:latest

WORKDIR /app

COPY go.mod ./

COPY . .

COPY html/ ./html/
COPY static/ ./static/

RUN go build -o ./happyYD

EXPOSE 8080

CMD ["./happyYD"]
