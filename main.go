package main

import (
	"fmt"
	"net/http"
	"sync"
)

const MAX_QNUM = 10

type QuestionPageData struct {
	QuestionID   int
	QtextKorean  string
	QtextEnglish string
	Anum         int
	Atype        string
}

type User struct {
	UserID   int
	RealName string
	NickName string
	Answers  map[int][]string //stores data for 내가 보는 나      qid -> Answers
	Guesses  map[int]string   //stores data for 내가 보는 너      qid -> realname
}

type CorrectAnswer struct {
	mu            sync.Mutex
	correctAnswer []int
}

var Questions []QuestionPageData
var Users = make(map[int]User)
var CAnswer CorrectAnswer

func mainHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "html/index.html")
}

func goodbyeHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "html/goodbye.html")
}

func init() {
	Questions = make([]QuestionPageData, MAX_QNUM+1)
	Questions[1] = QuestionPageData{
		QuestionID:   1,
		QtextKorean:  "나의 장점 3가지",
		QtextEnglish: "My three greatest strengths",
		Anum:         3,
		Atype:        "short",
	}
	Questions[2] = QuestionPageData{
		QuestionID:   2,
		QtextKorean:  "좋아하는 아티스트",
		QtextEnglish: "Favirote artists",
		Anum:         3,
		Atype:        "short",
	}
	Questions[3] = QuestionPageData{
		QuestionID:   3,
		QtextKorean:  "핫도그는 샌드위치 인가요?",
		QtextEnglish: "Is a hotdog a sandwich?",
		Anum:         1,
		Atype:        "long",
	}
	Questions[4] = QuestionPageData{
		QuestionID:   4,
		QtextKorean:  "나에게 거슬리는것, 나를 짜증나게 하는 것들",
		QtextEnglish: "What are your pet peeves?",
		Anum:         3,
		Atype:        "short",
	}
	Questions[5] = QuestionPageData{
		QuestionID:   5,
		QtextKorean:  "나를 표현하는 형용사 3가지",
		QtextEnglish: "3 adjectives that describe you the bset",
		Anum:         3,
		Atype:        "short",
	}
	Questions[6] = QuestionPageData{
		QuestionID:   6,
		QtextKorean:  "없으면 살 수 없는 습관이나 일과는 무엇인가요?",
		QtextEnglish: "What's your habit or daily routine that you cannot live without it?",
		Anum:         1,
		Atype:        "long",
	}
	Questions[7] = QuestionPageData{
		QuestionID:   7,
		QtextKorean:  "나에게 완벽한 날 이란?",
		QtextEnglish: "What's a perfect day for you?",
		Anum:         1,
		Atype:        "long",
	}
	Questions[8] = QuestionPageData{
		QuestionID:   8,
		QtextKorean:  "사람에게서 매력적으로 느껴지는 것은 무엇인가요?",
		QtextEnglish: "What traits do you admire in others?",
		Anum:         1,
		Atype:        "long",
	}
	Questions[9] = QuestionPageData{
		QuestionID:   9,
		QtextKorean:  "가장 좋아하는 성경 구절",
		QtextEnglish: "Favorite Bible verse?",
		Anum:         1,
		Atype:        "long",
	}
	Questions[10] = QuestionPageData{
		QuestionID:   10,
		QtextKorean:  "하고 싶은 말",
		QtextEnglish: "Something's on your mind?",
		Anum:         1,
		Atype:        "long",
	}
	for _, question := range Questions {
		fmt.Println(question.QtextKorean)
	}
	CAnswer.correctAnswer = make([]int, 0)
}

func main() {
	mux := http.NewServeMux()
	fs := http.FileServer(http.Dir("static"))
	mux.Handle("/static/", http.StripPrefix("/static/", fs))
	mux.HandleFunc("/submit-answers", submitAnswerHandler)
	mux.HandleFunc("/submit-name", submitNameHandler)
	mux.HandleFunc("/submit-guess", submitGuessHandler)
	mux.HandleFunc("/user/{userid}/guess/{guessid}", guessHandler)
	mux.HandleFunc("/user/{userid}/question/{questionid}", questionHandler)
	mux.HandleFunc("/user/{userid}/check-guesses", checkMyGuessesHandler)
	mux.HandleFunc("/user/{userid}/grade-guesses", gradeMyGuesssHandler)
	mux.HandleFunc("/user/{userid}/answer", answerHandler)
	mux.HandleFunc("/thank-you/", thankyouHandler)
	mux.HandleFunc("/goodbye", goodbyeHandler)
	mux.HandleFunc("/userid", userIDHandler)
	mux.HandleFunc("/{$}", mainHandler)

	http.ListenAndServe(":8080", mux)
}
