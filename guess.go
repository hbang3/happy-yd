package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

func guessHandler(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(r.PathValue("userid"))
	if err != nil {
		http.Error(w, "userID", http.StatusInternalServerError)
		return
	}

	guessID, err := strconv.Atoi(r.PathValue("guessid"))
	if err != nil {
		http.Error(w, "guessHandler", http.StatusInternalServerError)
		return
	}

	tmpl, err := template.ParseFiles("html/guess.html")
	if err != nil {
		http.Error(w, "Error preparing the template", http.StatusInternalServerError)
		return
	}

	RealNames := make([]string, 0)
	for _, i := range CAnswer.correctAnswer {
		RealNames = append(RealNames, Users[i].RealName)
	}
	fmt.Println(CAnswer.correctAnswer)

	data := struct {
		User        User
		NickName    string
		Guess       string
		Questions   []QuestionPageData
		RealNames   []string
		UserID      int
		GuessID     int
		NextGuessID int
		PrevGuessID int
	}{
		User:        Users[guessID],
		NickName:    Users[guessID].NickName,
		Guess:       Users[userID].Guesses[guessID],
		Questions:   Questions,
		RealNames:   RealNames,
		UserID:      userID,
		GuessID:     guessID,
		NextGuessID: min(len(CAnswer.correctAnswer), guessID+1),
		PrevGuessID: max(1, guessID-1),
	}

	tmpl.Execute(w, data)
}

func submitGuessHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Only POST method is allowed", http.StatusMethodNotAllowed)
		return
	}

	if err := r.ParseForm(); err != nil {
		http.Error(w, "Error parsing form", http.StatusBadRequest)
		return
	}
	userID, err := strconv.Atoi(r.Form.Get("userid"))
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	guessID, err := strconv.Atoi(r.Form.Get("guessid"))
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	selectedName := r.Form.Get("selectedname")
	if selectedName != "" {
		userGuess := Users[userID].Guesses
		userGuess[guessID] = selectedName

		Users[userID] = User{
			UserID:   userID,
			RealName: Users[userID].RealName,
			NickName: Users[userID].NickName,
			Answers:  Users[userID].Answers,
			Guesses:  userGuess,
		}
	}

	var redirectAddr string
	if r.FormValue("action") == "prev" {
		redirectAddr = fmt.Sprintf("/user/%d/guess/%d", userID, max(guessID-1, 1))
	} else if r.FormValue("action") == "next" {
		redirectAddr = fmt.Sprintf("/user/%d/guess/%d", userID, min(MAX_QNUM, guessID+1))
	} else if r.FormValue("action") == "submit" {
		redirectAddr = fmt.Sprintf("/user/%d/check-guesses", userID)
	}
	http.Redirect(w, r, redirectAddr, http.StatusSeeOther)
}

func checkMyGuessesHandler(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(r.PathValue("userid"))
	if err != nil {
		http.Error(w, "Internal Server Error: check my guess", http.StatusInternalServerError)
		return
	}

	tmpl, err := template.ParseFiles("html/checkmyguess.html")
	if err != nil {
		http.Error(w, "Error preparing the template", http.StatusInternalServerError)
		return
	}

	data := struct {
		Users   map[int]User
		User    User
		UserID  int
		Guesses map[int]string
	}{
		Users:   Users,
		User:    Users[userID],
		UserID:  userID,
		Guesses: Users[userID].Guesses,
	}
	tmpl.Execute(w, data)
}

func gradeMyGuesssHandler(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(r.PathValue("userid"))
	if err != nil {
		http.Error(w, "Internal Server Error: check my guess", http.StatusInternalServerError)
		return
	}

	tmpl, err := template.ParseFiles("html/grademyguess.html")
	if err != nil {
		http.Error(w, "Error preparing the template", http.StatusInternalServerError)
		return
	}

	data := struct {
		Users   map[int]User
		User    User
		UserID  int
		Guesses map[int]string
	}{
		Users:   Users,
		User:    Users[userID],
		UserID:  userID,
		Guesses: Users[userID].Guesses,
	}
	tmpl.Execute(w, data)
}
