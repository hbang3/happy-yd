import os
import base64

def encode_images_to_base64_html(directory):
    html_image_tags = []

    for filename in os.listdir(directory):
        if filename.endswith('.jpg'):
            # Construct the full file path
            filepath = os.path.join(directory, filename)

            with open(filepath, 'rb') as image_file:
                encoded_string = base64.b64encode(image_file.read()).decode('utf-8')

                img_tag = f'<img src="data:image/jpeg;base64,{encoded_string}" style="display: none;"/>'
                html_image_tags.append(img_tag)

    return html_image_tags

directory = 'static/images'
html_images = encode_images_to_base64_html(directory)
for img_tag in html_images:
    print(img_tag)
