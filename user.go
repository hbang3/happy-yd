package main

import (
	"fmt"
	"html/template"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
)

var userIDCounter int32

func userIDHandler(w http.ResponseWriter, r *http.Request) {
	userAnimal := randAdjAnimal()
	tmpl, err := template.ParseFiles("html/userid.html")
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	data := struct {
		UserAnimal string
	}{
		UserAnimal: userAnimal,
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func submitNameHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Only POST method is allowed", http.StatusMethodNotAllowed)
		return
	}

	if err := r.ParseForm(); err != nil {
		http.Error(w, "Error parsing form", http.StatusBadRequest)
		return
	}

	nickName := r.Form.Get("useranimal")
	if nickName == "" {
		http.Error(w, "Invlaid Name ", http.StatusInternalServerError)
		return
	}

	realName := r.Form.Get("realname")
	if realName == "" {
		http.Error(w, "Invlaid Name ", http.StatusInternalServerError)
		return
	}

	userId := int(assignUserId())
	init_answers := make(map[int][]string)
	for _, question := range Questions {
		init_answers[question.QuestionID] = make([]string, question.Anum)
	}

	if _, ok := Users[userId]; ok {
		http.Error(w, "Duplicate User ID", http.StatusInternalServerError)
		return
	}

	Users[userId] = User{
		UserID:   userId,
		RealName: realName,
		NickName: nickName,
		Answers:  init_answers,
		Guesses:  make(map[int]string),
	}

	CAnswer.mu.Lock()
	CAnswer.correctAnswer = append(CAnswer.correctAnswer, userId)
	rand.Shuffle(len(CAnswer.correctAnswer), func(i, j int) {
		CAnswer.correctAnswer[i], CAnswer.correctAnswer[j] = CAnswer.correctAnswer[j], CAnswer.correctAnswer[i]
	})
	CAnswer.mu.Unlock()
	fmt.Println(CAnswer.correctAnswer)

	redirectAddr := fmt.Sprintf("/user/%d/question/1", userId)
	http.Redirect(w, r, redirectAddr, http.StatusSeeOther)
}

func thankyouHandler(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("html/thankyou.html")
	if err != nil {
		http.Error(w, "Error preparing the template", http.StatusInternalServerError)
		return
	}

	pathParts := strings.Split(r.URL.Path, "/")
	userID, err := strconv.Atoi(pathParts[2])
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}
	if _, ok := Users[userID]; !ok {
		http.Error(w, "UserID not found", http.StatusBadRequest)
		return
	}

	data := struct {
		NickName string
		UserID   int
	}{
		NickName: Users[userID].NickName,
		UserID:   userID,
	}

	tmpl.Execute(w, data)
}
