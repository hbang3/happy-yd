package main

import (
	"html/template"
	"net/http"
	"strconv"
)

func answerHandler(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(r.PathValue("userid"))
	if err != nil {
		http.Error(w, "Invalid User ID", http.StatusBadRequest)
		return
	}

	if _, ok := Users[userID]; !ok {
		http.Error(w, "User not found", http.StatusBadRequest)
		return
	}

	data := struct {
		User      User
		Answers   map[int][]string
		Questions []QuestionPageData
	}{
		User:      Users[userID],
		Answers:   Users[userID].Answers,
		Questions: Questions,
	}
	tmpl, err := template.ParseFiles("html/answer.html")
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	tmpl.Execute(w, data)
}
